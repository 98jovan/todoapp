import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:todoapp/main.dart';

Future<void> logout(WidgetTester tester) async {
  await Future<void>.delayed(const Duration(seconds: 2));
  await tester.tap(
    find.byKey(
      const ValueKey('signOut'),
    ),
  );

  await Future<void>.delayed(const Duration(seconds: 2));
  await tester.pumpAndSettle();
}

void main() {
  final binding = IntegrationTestWidgetsFlutterBinding.ensureInitialized();
  binding.framePolicy = LiveTestWidgetsFlutterBindingFramePolicy.fullyLive;
  WidgetsFlutterBinding.ensureInitialized();
  group('todoapp', () {
    final timeBasedEmail = '${DateTime.now().microsecondsSinceEpoch}@test.com';
    testWidgets('create new todo item', (tester) async {
      await Firebase.initializeApp();
      await tester.pumpWidget(const MyApp());
      await tester.pumpAndSettle();

      await tester.enterText(
        find.byKey(const ValueKey('username')),
        timeBasedEmail,
      );
      await tester.enterText(find.byKey(const ValueKey('password')), '123456');
      await tester.tap(find.byKey(const ValueKey('createAccount')));
      await Future<void>.delayed(const Duration(seconds: 2));
      await tester.pumpAndSettle();

      expect(find.text('Add Todo Here:'), findsOneWidget);

      for (var i = 0; i < 3; i++) {
        await tester.enterText(
          find.byKey(const ValueKey('addField')),
          'Test$i',
        );
        final Finder button = find.byKey(const ValueKey('addButton'));
        await tester.tap(button);
        await Future<void>.delayed(const Duration(seconds: 1));
        await tester.pumpAndSettle();
      }

      expect(find.text('Test0'), findsOneWidget);

      await logout(tester);
    });
  });
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:todoapp/models/todo.dart';

class Database {
  final FirebaseFirestore firestore;

  Database({required this.firestore});

  Stream<List<TodoModel>> streamTodos({required String uid}) {
    return firestore
        .collection('todos')
        .doc(uid)
        .collection('todos')
        .where('done', isEqualTo: false)
        .snapshots()
        .map((event) {
      final List<TodoModel> retVal = [];
      for (final DocumentSnapshot doc in event.docs) {
        retVal.add(TodoModel.fromSnapshot(doc));
      }
      return retVal;
    });
  }

  Future<void> addTodo({required String uid, required String content}) async {
    firestore.collection('todos').doc(uid).collection('todos').add({
      'content': content,
      'done': false,
    });
  }

  Future<void> updateTodo({required String uid, required String todoId}) async {
    firestore
        .collection('todos')
        .doc(uid)
        .collection('todos')
        .doc(todoId)
        .update({
      'done': true,
    });
  }
}

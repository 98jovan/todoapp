import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:todoapp/services/auth.dart';

class LoginScreen extends StatefulWidget {
  final FirebaseAuth auth;
  final FirebaseFirestore firestore;

  const LoginScreen({super.key, required this.auth, required this.firestore});
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(60.0),
          child: Builder(
            builder: (BuildContext context) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextFormField(
                    key: const ValueKey('username'),
                    textAlign: TextAlign.center,
                    decoration: const InputDecoration(hintText: 'Username'),
                    controller: _emailController,
                  ),
                  TextFormField(
                    obscureText: true,
                    key: const ValueKey('password'),
                    textAlign: TextAlign.center,
                    decoration: const InputDecoration(hintText: 'Password'),
                    controller: _passwordController,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    key: const ValueKey('signIn'),
                    onPressed: () async {
                      final bool retVal = await Auth(auth: widget.auth).signIn(
                        email: _emailController.text,
                        password: _passwordController.text,
                      );
                      if (retVal) {
                        _emailController.clear();
                        _passwordController.clear();
                      } else {
                        if (!mounted) return;
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text('An error occured'),
                          ),
                        );
                      }
                    },
                    child: const Text('Sign In'),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    key: const ValueKey('createAccount'),
                    onTap: () async {
                      final bool retVal =
                          await Auth(auth: widget.auth).createAccount(
                        email: _emailController.text,
                        password: _passwordController.text,
                      );
                      if (retVal) {
                        _emailController.clear();
                        _passwordController.clear();
                      } else {
                        if (!mounted) return;
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text('An error occured'),
                          ),
                        );
                      }
                    },
                    child: const Text('Create Account'),
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

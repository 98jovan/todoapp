import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:todoapp/models/todo.dart';
import 'package:todoapp/services/auth.dart';
import 'package:todoapp/services/database.dart';
import 'package:todoapp/widgets/todo_card.dart';

class HomeScreen extends StatefulWidget {
  final FirebaseAuth auth;
  final FirebaseFirestore firestore;

  const HomeScreen({required this.auth, required this.firestore});

  @override
  State<HomeScreen> createState() => _HomeState();
}

class _HomeState extends State<HomeScreen> {
  final TextEditingController _todoController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Todo App'),
        centerTitle: true,
        actions: [
          GestureDetector(
            key: const ValueKey('signOut'),
            child: const Padding(
              padding: EdgeInsets.all(18.0),
              child: Text('LogOut'),
            ),
            onTap: () {
              Auth(auth: widget.auth).logOut();
            },
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          const SizedBox(
            height: 20,
          ),
          const Text(
            'Add Todo Here:',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          Card(
            margin: const EdgeInsets.all(20),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      key: const ValueKey('addField'),
                      controller: _todoController,
                    ),
                  ),
                  IconButton(
                    key: const ValueKey('addButton'),
                    icon: const Icon(Icons.add),
                    onPressed: () {
                      if (_todoController.text != '') {
                        setState(() {
                          Database(firestore: widget.firestore).addTodo(
                            uid: widget.auth.currentUser!.uid,
                            content: _todoController.text,
                          );
                          _todoController.clear();
                        });
                      }
                    },
                  )
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            'Your Todos',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          Expanded(
            child: StreamBuilder(
              stream: Database(firestore: widget.firestore)
                  .streamTodos(uid: widget.auth.currentUser!.uid),
              builder: (
                BuildContext context,
                AsyncSnapshot<List<TodoModel>> snapshot,
              ) {
                if (snapshot.connectionState == ConnectionState.active) {
                  if (snapshot.data!.isEmpty) {
                    return const Center(
                      child: Text("You don't have any unfinished Todos"),
                    );
                  }
                  return ListView.builder(
                    itemCount: snapshot.data?.length,
                    itemBuilder: (_, index) {
                      return TodoCard(
                        firestore: widget.firestore,
                        uid: widget.auth.currentUser!.uid,
                        todo: snapshot.data![index],
                      );
                    },
                  );
                } else {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}

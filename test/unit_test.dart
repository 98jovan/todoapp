import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:todoapp/services/auth.dart';

import 'unit_test.mocks.dart';

@GenerateMocks([FirebaseAuth, UserCredential])
void main() {
  late Auth auth;
  late MockUserCredential _mockCredentials;
  late MockFirebaseAuth _mockFirebase;

  setUp(() {
    _mockFirebase = MockFirebaseAuth();
    _mockCredentials = MockUserCredential();
    auth = Auth(auth: _mockFirebase);
  });
  group('auth', () {
    test("create account exception", () async {
      when(
        _mockFirebase.createUserWithEmailAndPassword(
          email: "987jovan@gmail.com",
          password: "123456",
        ),
      ).thenAnswer((_) => throw FirebaseAuthException(code: 'Error'));
      expect(
        await auth.createAccount(
          email: "987jovan@gmail.com",
          password: "123456",
        ),
        false,
      );
    });
    test("create account success", () async {
      when(
        _mockFirebase.createUserWithEmailAndPassword(
          email: "987jovan@gmail.com",
          password: "123456",
        ),
      ).thenAnswer(
        (_) async => _mockCredentials,
      );
      expect(
        await auth.createAccount(
          email: "987jovan@gmail.com",
          password: "123456",
        ),
        true,
      );
    });
    test("signin account", () async {
      when(
        _mockFirebase.signInWithEmailAndPassword(
          email: "987jovan@gmail.com",
          password: "123456",
        ),
      ).thenAnswer((_) async => _mockCredentials);
      expect(
        await auth.signIn(
          email: "987jovan@gmail.com",
          password: "123456",
        ),
        true,
      );
    });

    test("signin account exception", () async {
      when(
        _mockFirebase.signInWithEmailAndPassword(
          email: "987jovan@gmail.com",
          password: "123456",
        ),
      ).thenAnswer((_) => throw FirebaseAuthException(code: 'Error'));
      expect(
        await auth.signIn(
          email: "987jovan@gmail.com",
          password: "123456",
        ),
        false,
      );
    });
    test("logout", () async {
      when(
        _mockFirebase.signOut(),
      ).thenAnswer((_) async => true);
      expect(
        await auth.logOut(),
        true,
      );
    });
    test("logout exception", () async {
      when(
        _mockFirebase.signOut(),
      ).thenAnswer((_) => throw FirebaseAuthException(code: 'Error'));
      expect(
        await auth.logOut(),
        false,
      );
    });
  });
}
